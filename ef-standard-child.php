<?php

/*
Plugin Name: EF-Standard-Child
Description: Design, Funktionen und Einstellungen für das EF-Standard Theme.
Version: 0.1.2
Author: Timo Klemm
Author URI: https://github.com/team-ok/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


// add less fragments to the compiler
add_action( 'beans_uikit_enqueue_scripts', 'ef_standard_child_enqueue_less_fragments', 99 );

function ef_standard_child_enqueue_less_fragments() {

	$fragment = plugin_dir_path( __FILE__ ) . 'styles/ef-standard-child.less';
	// beans compiler bug when adding zero-sized files
	if ( file_exists( $fragment ) && filesize( $fragment ) > 0 ) {

    	beans_compiler_add_fragment( 'uikit', plugin_dir_path( __FILE__ ) . 'styles/ef-standard-child.less', 'less' );
    }
}

// load scripts
add_action( 'wp_enqueue_scripts', 'ef_standard_child_scripts', 15);
function ef_standard_child_scripts(){

	$js = plugin_dir_path( __FILE__ ) . 'scripts/ef-standard-child.js';
	if ( file_exists( $js ) && filesize( $js) > 0 ){

		wp_enqueue_script( 'ef-standard-child', plugin_dir_url( __FILE__ ) . 'scripts/ef-standard-child.js', array( 'jquery' ), filemtime( plugin_dir_path( __FILE__ ) . 'scripts/ef-standard-child.js'), true );
	}

}

// replace hero template
add_filter( 'ef_hero_template', function( $hero ) {

    if ( file_exists( plugin_dir_path( __FILE__ ) . 'template-parts/ef-hero.php') ){
        
        return plugin_dir_path( __FILE__ ) . 'template-parts/ef-hero.php';
    }

    return $hero;
});

// translate previous/next post link texts
add_filter( 'beans_previous_text[_post_navigation]_output', function(){
	return 'Vorheriger Artikel';
});
add_filter( 'beans_next_text[_post_navigation]_output', function(){
	return 'Nächster Artikel';
});

add_filter( 'beans_previous_text[_posts_pagination]_output', function(){
	return 'Vorherige Seite';
});
add_filter( 'beans_next_text[_posts_pagination]_output', function(){
	return 'Nächste Seite';
});