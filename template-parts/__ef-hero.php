<?php
/**
 *
 * Hero Template
 *
 */

$event_data = array(
	'ef_event_type' => get_theme_mod( 'ef_event_type' ), 
	'ef_event_date' =>  get_theme_mod( 'ef_event_date' ),
	'ef_event_venue' => get_theme_mod( 'ef_event_venue' )
);

?>
<div class="uk-block">
	<div class="uk-container uk-container-center">
		<div class="uk-text-center">
			<h1><a href="#" data-ef-scroll-to-content><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="subline upper"><?php bloginfo( 'description' ); ?></h2>
			<p class="ef-event-data ef-highlight-text"><?php echo esc_html( implode( ', ', array_filter( $event_data ) ) ); ?></p>
			<p class="ef-3d-button-wrapper">
				<a class="ef-3d-button anmeldung down" href="information-teilnahme/#anmeldung">Jetzt anmelden<span class="back">Zur Anmeldung</span></a>
				<a class="ef-3d-button broschuere up" href="information-teilnahme/#programm" data-content="Zum Download">Broschüre herunterladen<span class="back">Zum Download</span></a>
			</p>
		</div>
	</div>
</div>